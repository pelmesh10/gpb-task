module.exports = {
  transpileDependencies: [
    'vuetify',
  ],
  devServer: {
    overlay: false,
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? '/gpb-task/'
    : '/',
};
